#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <math.h>
#include "BSTree.h"


//=============================================================================
/* Skapar en tradnod med det givna datat genom att allokera minne for noden */
static struct treeNode* createNode(int data)
{
    struct treeNode* newnode = (struct treeNode*)malloc(sizeof(struct treeNode));

    if (newnode != NULL)
    {
        (*newnode).data = data;
        (*newnode).left = NULL;
        (*newnode).right = NULL;
    }
    return newnode;
}
//=============================================================================
/* Help function for writeSortedToArray */
static void inorder_array(const BSTree tree, int *arr, int *arr_pos)
{
    if(tree != NULL)
    {
        if((*tree).left != NULL)
        {
            inorder_array((*tree).left, arr, arr_pos);
        }

        arr[*arr_pos] = (*tree).data;
        (*arr_pos)++;

        if((*tree).right != NULL)
        {
            inorder_array((*tree).right, arr, arr_pos);
        }
    }
}
//=============================================================================
/* Returnerar en dynamiskt allokerad array som innehaller tradets data sorterat */
static int* writeSortedToArray(const BSTree tree)
{
    if(tree == NULL)
    {
        return NULL;
    }

    int *arr = (int*)malloc(sizeof(int) * numberOfNodes(tree));

    //should check if allocation worked (arr != NULL)

    int arr_pos = 0;

    inorder_array(tree, arr, &arr_pos);    

    return arr;
}
//=============================================================================
/* Bygger upp ett sorterat, balanserat trad fran en sorterad array */
static void buildTreeSortedFromArray(BSTree* tree, const int arr[], int size)
{
    int root;
    int first_size;
    int second_half;
    int second_size;

    if(size == 0)
    {
        return;
    }
    if(*tree != NULL)
    {
        return;
    }

    if(size % 2 != 0)
    {
        root = (size-1)/2;
        first_size = (size-1)/2;
        second_half = (size+1)/2;
        second_size = (size-1)/2;
    }
    else
    {
        root = size/2-1;
        first_size = size/2-1;
        second_half = size/2;
        second_size = size/2;
    }

    *tree = createNode(arr[root]);
    buildTreeSortedFromArray(&(**tree).left, arr, first_size);
    buildTreeSortedFromArray(&(**tree).right, &arr[second_half], second_size);
}
//=============================================================================
/* Skapar ett tomt trad */
BSTree emptyTree(void)
{
    return NULL;
}
//=============================================================================
/* Returnerar 1 ifall tradet ar tomt, 0 annars */
int isEmpty(const BSTree tree)
{
    if (tree == NULL)
    {
        return 1;
    }

    return 0;
}
//=============================================================================
/* Satter in 'data' sorterat i *tree */
void insertSorted(BSTree* tree, int data)
{
    if(*tree == NULL)
    {
        *tree = createNode(data);
    }
    else if(data == (**tree).data)
    {
        fprintf(stderr, "Data is already in tree");
    }
    else if(data < (**tree).data)
    {
        if((**tree).left != NULL)
        {
            insertSorted(&(**tree).left, data);
        }
        else
        {
            (**tree).left = createNode(data);
        }    
    }
    else if(data > (**tree).data)
    {
        if((**tree).right != NULL)
        {
            insertSorted(&(**tree).right, data);
        }
        else
        {
            (**tree).right = createNode(data);
        }
    }

    assert(find(*tree, data) == 1);
}
//=============================================================================
/* Utskriftsfunktioner */
void printPreorder(const BSTree tree, FILE *textfile)
{
    if(tree != NULL)
    {
        fprintf(textfile, "%d\n", (*tree).data);

        if((*tree).left != NULL)
        {
            printPreorder((*tree).left, textfile);
        }
        if((*tree).right != NULL)
        {
            printPreorder((*tree).right, textfile);   
        }
    }
}
//=============================================================================
void printInorder(const BSTree tree, FILE *textfile)
{
    if(tree != NULL)
    {
        if((*tree).left != NULL)
        {
            printInorder((*tree).left, textfile);
        }

        fprintf(textfile, "%d\n", (*tree).data);

        if((*tree).right != NULL)
        {
            printInorder((*tree).right, textfile);
        }
    }
}
//=============================================================================
void printPostorder(const BSTree tree, FILE *textfile)
{
    if(tree != NULL)
    {
        if((*tree).left != NULL)
        {
            printPostorder((*tree).left, textfile);
        }
        if((*tree).right != NULL)
        {
            printPostorder((*tree).right, textfile);
        }

        fprintf(textfile, "%d\n", (*tree).data);
    }
}
//=============================================================================
/* Returnerar 1 om 'data' finns i tree, 0 annars */
int find(const BSTree tree, int data)
{
    if(tree == NULL)
    {
        return 0;
    }
    else
    {
        if((*tree).data == data)
        {
            return 1;
        }
        else if((data < (*tree).data) && ((*tree).left != NULL))
        {
            return find((*tree).left, data);
        }
        else if((data > (*tree).data) && ((*tree).right != NULL))
        {
            return find((*tree).right, data);
        }
    }

    return 0;
}
//=============================================================================
/* Tar bort 'data' fran tradet om det finns */
void removeElement(BSTree* tree, int data)
{
    int size;
    int *arr;
    int i = 0;

    if(find(*tree, data) == 0)
    {
        return;
    }

    size = numberOfNodes(*tree);
    arr = writeSortedToArray(*tree);
    freeTree(tree);

    while(arr[i] != data)
    {
        i++;
    }

    //writes over the data that is to be removed by shifting the elements after 1 step to the left
    while(i < size-1)
    {
        arr[i] = arr[i+1];
        i++;
    }

    buildTreeSortedFromArray(tree, arr, size-1);
    free(arr);
}
//=============================================================================
/* Returnerar hur manga noder som totalt finns i tradet */
int numberOfNodes(const BSTree tree)
{
    int i = 0;

    if(tree == NULL)
    {
        return 0;
    }
    i++;
    if((*tree).left != NULL)
    {
        i += numberOfNodes((*tree).left);
    }
    if((*tree).right != NULL)
    {
        i += numberOfNodes((*tree).right);
    }

    return i;
}
//=============================================================================
/* Returnerar hur djupt tradet ar */
int depth(const BSTree tree)
{
    int i = 0;

    if(tree == NULL)
    {
        return 0;
    }

    i++;

    if(depth((*tree).left) >= depth((*tree).right))
    {
        i += depth((*tree).left);
    }
    else if(depth((*tree).left) < depth((*tree).right))
    {
        i += depth((*tree).right);
    } 

    return i;
}
//=============================================================================
/* Returnerar minimidjupet for tradet */
/* need to compile with: "gcc -lm ..." for log etc to work */
int minDepth(const BSTree tree)
{
    int mdepth = 0;
    int nodes = numberOfNodes(tree);

    mdepth = ceil(log((float)(nodes+1))/log(2.0));

    return mdepth;
}
//=============================================================================
/* Balansera tradet sa att depth(tree) == minDepth(tree) */
void balanceTree(BSTree* tree)
{
    if(*tree == NULL)
    {
        return;
    }

    int size = numberOfNodes(*tree);
    int *arr = writeSortedToArray(*tree);

    freeTree(tree);
    buildTreeSortedFromArray(tree, arr, size);
    free(arr);

    assert(numberOfNodes(*tree) == size);
    assert(depth(*tree) == minDepth(*tree));
}
//=============================================================================
/* Tom tradet och frigor minnet for de olika noderna */
void freeTree(BSTree* tree)
{
    if(*tree != NULL)
    {
        if((**tree).left != NULL)
        {
            freeTree(&(**tree).left);
        }
        if((**tree).right != NULL)
        {
            freeTree(&(**tree).right);
        }
        free(*tree);
        *tree = NULL;
    }

    assert(isEmpty(*tree));
}
//=============================================================================
float numberofchildren(const BSTree tree)
{
    float children = 0;

    if((*tree).left != NULL)
    {
        children++;
        children += numberofchildren((*tree).left);
    }
    if((*tree).right != NULL)
    {
        children++;
        children += numberofchildren((*tree).right);
    }

    return children;
}
//=============================================================================
float averageNumberOfChildren(const BSTree tree)
{
    assert(isEmpty(tree) != 1);

    float children = numberofchildren(tree);
    float nodes = (float)numberOfNodes(tree);
    
    return children/nodes;
}
